# SauceLabs Docker Proxy

This project builds and stores a simple docker container for running a SauceLabs docker proxy. 

## Getting Started

Download the image from GitLab
`docker pull registry.gitlab.com/rice-patrick/saucelabs-proxy-docker`

Simply run the docker container using several environment variables.
`docker run -e SAUCE_USERNAME=<your username> -e SAUCE_ACCESS_KEY=<your API key> -e TUNNEL_NAME=<name for tunnel> /rice-patrick/saucelabs-proxy-docker`
